<?php

namespace GuzzleExtension\ResponseParser;

use GuzzleExtension\Interfaces\IDeserializer;
use Psr\Http\Message\ResponseInterface;

class BodyDeserialize extends BaseParser
{
    /** @var IDeserializer */
    private $deserializer;

    /**
     * Json constructor.
     * @param IDeserializer $deserializer
     */
    public function __construct(IDeserializer $deserializer)
    {
        $this->deserializer = $deserializer;
    }

    protected function processSuccessResponse(ResponseInterface $response, $responseClass)
    {
        return $this->deserializer->deserialize($response->getBody()->getContents(), $responseClass);
    }
}