<?php

namespace GuzzleExtension\ResponseParser;

use Psr\Http\Message\ResponseInterface;

class RedirectIsSuccess extends BaseParser
{
    protected function getSuccessCode()
    {
        return 302;
    }
    
    protected function processSuccessResponse(ResponseInterface $response, $responseClass)
    {
        return new $responseClass;
    }

    final protected function getLocation(ResponseInterface $response)
    {
        return $response->getHeaderLine('Location');
    }
}