<?php

namespace GuzzleExtension\ResponseParser;

use GuzzleExtension\Exceptions\UnsuccessResponseException;
use GuzzleExtension\Interfaces\IResponse;
use Psr\Http\Message\ResponseInterface;

class BaseParser extends AbstractParser
{
    /**
     * @return int
     */
    protected function getSuccessCode()
    {
        return 200;
    }

    protected function isSuccess(ResponseInterface $response)
    {
        return $response->getStatusCode() == $this->getSuccessCode();
    }

    protected function processFailResponse(ResponseInterface $response)
    {
        throw new UnsuccessResponseException(
            sprintf(
                'Receive unsuccess response. Success code is: "%s", Code: "%s", Headers: "%s". Body: "%s"',
                get_class($this) . $this->getSuccessCode(),
                $response->getStatusCode(),
                var_export($response->getHeaders(), true),
                $response->getBody()->getContents()
            )
        );
    }

    protected function processSuccessResponse(ResponseInterface $response, $responseClass)
    {
        dump($response->getBody()->getContents());

        return new $responseClass;
    }
}