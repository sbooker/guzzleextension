<?php

namespace GuzzleExtension\ResponseParser;

use GuzzleExtension\Interfaces\IResponse;
use GuzzleExtension\Interfaces\IResponseParser;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractParser implements IResponseParser
{
    final public function parse(ResponseInterface $response, $responseClass)
    {
        return
            $this->isSuccess($response)
                ? $this->processSuccessResponse($response, $responseClass)
                : $this->processFailResponse($response);
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    abstract protected function isSuccess(ResponseInterface $response);

    /**
     * @param ResponseInterface $response
     * @param $responseClass
     * @return IResponse
     */
    abstract protected function processSuccessResponse(ResponseInterface $response, $responseClass);

    /**
     * @param ResponseInterface $response
     * @return IResponse
     */
    abstract protected function processFailResponse(ResponseInterface $response);
}