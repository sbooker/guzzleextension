<?php

namespace GuzzleExtension\Deserializer;

use GuzzleExtension\Exceptions\ParserException;
use GuzzleExtension\Interfaces\IDeserializer;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class XPath implements IDeserializer
{
    /** @var Crawler */
    private $crawler;

    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    /** @var array */
    private $propertyToXpathMap;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(Crawler $crawler, PropertyAccessorInterface $propertyAccessor, array $propertyToXpathMap, LoggerInterface $logger)
    {
        $this->crawler = $crawler;
        $this->propertyAccessor = $propertyAccessor;
        $this->propertyToXpathMap = $propertyToXpathMap;
        $this->logger = $logger;
    }

    public function deserialize($string, $class)
    {
        $this->logger->debug(sprintf('Try parse string: "%s"', $string));

        $this->crawler->addHtmlContent($string);
        $object = new $class();
        
        foreach ($this->propertyToXpathMap as $property => $xpath) {
            $this->propertyAccessor->setValue(
                $object, 
                $property,
                $this->parseRequiredValue($xpath)
            );
        }
        
        return $object;
    }

    /**
     * @param $xpath
     * @return string
     * @throws ParserException
     */
    private function parseRequiredValue($xpath)
    {
        try {
            $value = $this->crawler->filterXPath($xpath)->text();
            if (empty($value)) {
                throw new \InvalidArgumentException();
            }

            return $value;
        } catch (\InvalidArgumentException $e) {
            throw new ParserException(sprintf('Can not parse required value with XPATH "%s"', $xpath));
        }
    }
}