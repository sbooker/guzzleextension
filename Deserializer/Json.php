<?php

namespace GuzzleExtension\Deserializer;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IDeserializer;
use GuzzleExtension\Interfaces\IResponse;
use JMS\Serializer\SerializerInterface;

class Json implements IDeserializer
{
    /** @var SerializerInterface */
    private $deserializer;

    public function __construct(SerializerInterface $deserializer)
    {
        $this->deserializer = $deserializer;
    }

    public function deserialize($string, $class)
    {
        Assert::isInstance($class, IResponse::class);
        return $this->deserializer->deserialize($string, $class, 'json');
    }
}