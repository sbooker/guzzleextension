<?php

namespace GuzzleExtension\HeaderBuilder;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IHeaderBuilder;
use GuzzleExtension\Interfaces\IRequest;

final class Composite implements IHeaderBuilder
{
    /** @var IHeaderBuilder[] */
    private $builderList = [];

    public function __construct(array $builderList)
    {
        Assert::allItemsInstancesOf($builderList, IHeaderBuilder::class);
        $this->builderList = $builderList;
    }

    /**
     * @param IRequest $request
     * @return array
     */
    public function build(IRequest $request)
    {
        $result = [];
        foreach ($this->builderList as $builder) {
            $result = array_merge($result, $builder->build($request));
        }

        return $result;
    }
}