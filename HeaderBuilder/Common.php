<?php

namespace GuzzleExtension\HeaderBuilder;

use GuzzleExtension\Interfaces\IHeaderBuilder;
use GuzzleExtension\Interfaces\IRequest;

final class Common implements IHeaderBuilder
{
    /** @var array */
    private $map;

    public function __construct(array $map = [])
    {
        $this->map = $map;
    }

    public function build(IRequest $request)
    {
        return $this->map;
    }
}