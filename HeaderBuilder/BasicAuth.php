<?php

namespace GuzzleExtension\HeaderBuilder;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IAuthRequest;
use GuzzleExtension\Interfaces\IHeaderBuilder;
use GuzzleExtension\Interfaces\IRequest;

final class BasicAuth implements IHeaderBuilder
{
    public function build(IRequest $request)
    {
        Assert::isInstance($request, IAuthRequest::class);
        /** @var IAuthRequest $request */
        return [
            'Authorization' => 
                        sprintf(
                            'Basic %s',
                            base64_encode(
                                sprintf('%s:%s', $request->getLogin(), $request->getPassword()))
                        )
        ];
    }
}