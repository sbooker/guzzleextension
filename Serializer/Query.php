<?php

namespace GuzzleExtension\Serializer;

use GuzzleExtension\Interfaces\IRequest;

final class Query extends Json
{
    public function serialize(IRequest $request)
    {
        return
            http_build_query(
                json_decode(
                    parent::serialize($request),
                    true
                ),
                '',
                '&',
                PHP_QUERY_RFC3986
            );
    }
}