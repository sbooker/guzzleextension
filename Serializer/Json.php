<?php

namespace GuzzleExtension\Serializer;

use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\ISerializer;
use JMS\Serializer\SerializerInterface;

class Json implements ISerializer
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function serialize(IRequest $request)
    {
        return $this->serializer->serialize($request, 'json');
    }
}