<?php

namespace GuzzleExtension\RequestBuilder;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IHeaderBuilder;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\ISerializer;
use GuzzleExtension\Serializer\Query;

final class Get extends AbstractBuilder
{
    public function __construct(ISerializer $serializer, IHeaderBuilder $headerBuilder)
    {
        Assert::isInstance($serializer, Query::class);
        parent::__construct($serializer, $headerBuilder);
    }

    protected function getMethod()
    {
        return 'GET';
    }

    protected function buildUri(IRequest $request, $url)
    {
        return
            parent::buildUri($request, $url)
                ->withQuery(
                    $this->serialize($request)
                );
    }

    protected function buildBody(IRequest $request)
    {
        return null;
    }
}