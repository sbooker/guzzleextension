<?php

namespace GuzzleExtension\RequestBuilder;

use GuzzleExtension\Interfaces\IHeaderBuilder;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\IRequestBuilder;
use GuzzleExtension\Interfaces\ISerializer;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;

abstract class AbstractBuilder implements IRequestBuilder
{
    /** @var ISerializer */
    private $serializer;

    /** @var IHeaderBuilder */
    private $headerBuilder;

    public function __construct(ISerializer $serializer, IHeaderBuilder $headerBuilder)
    {
        $this->serializer = $serializer;
        $this->headerBuilder = $headerBuilder;
    }

    /**
     * @return string
     */
    abstract protected function getMethod();

    /**
     * @param IRequest $request
     * @param string $url
     * @return Uri
     */
    protected function buildUri(IRequest $request, $url)
    {
        return new Uri($url);
    }

    /**
     * @param IRequest $request
     * @return string | null
     */
    abstract protected function buildBody(IRequest $request);

    final public function build(IRequest $request, $url)
    {
        return
            new Request(
                $this->getMethod(),
                $this->buildUri($request, $url),
                $this->headerBuilder->build($request),
                $this->buildBody($request)
            );
    }

    /**
     * @param IHeaderBuilder $headerBuilder
     * @return AbstractBuilder
     */
    public function setHeaderBuilder(IHeaderBuilder $headerBuilder)
    {
        $this->headerBuilder = $headerBuilder;
        return $this;
    }
    

    /**
     * @param IRequest $request
     * @return string
     */
    final protected function serialize(IRequest $request)
    {
        return $this->serializer->serialize($request);
    }
}