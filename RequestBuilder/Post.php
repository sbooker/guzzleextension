<?php

namespace GuzzleExtension\RequestBuilder;

use GuzzleExtension\Interfaces\IRequest;

final class Post extends AbstractBuilder
{
    protected function getMethod()
    {
        return 'POST';
    }

    protected function buildBody(IRequest $request)
    {
        return $this->serialize($request);
    }
}