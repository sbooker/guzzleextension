<?php

namespace GuzzleExtension\Interfaces;

interface IResponse
{
    /**
     * @return bool
     */
    public function isSuccess();
}