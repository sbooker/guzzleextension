<?php

namespace GuzzleExtension\Interfaces;

use GuzzleExtension\Exceptions\RegistryException;

interface IRequestBuilderRegistry
{
    /**
     * @param IRequest $request
     * @throws RegistryException
     * @return IRequestBuilder
     */
    public function getBuilder(IRequest $request);
}