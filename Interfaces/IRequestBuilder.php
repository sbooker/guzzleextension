<?php

namespace GuzzleExtension\Interfaces;

use Psr\Http\Message\RequestInterface;

interface IRequestBuilder
{
    /**
     * @param IRequest $request
     * @param string $url
     * @return RequestInterface
     */
    public function build(IRequest $request, $url);
}