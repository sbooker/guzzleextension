<?php

namespace GuzzleExtension\Interfaces;

interface ISerializer
{
    /**
     * @param IRequest $request
     * @return string
     */
    public function serialize(IRequest $request);
}