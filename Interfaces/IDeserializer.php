<?php

namespace GuzzleExtension\Interfaces;

interface IDeserializer
{
    /**
     * @param string $string
     * @param string $class
     * @return IResponse
     */
    public function deserialize($string, $class);
}