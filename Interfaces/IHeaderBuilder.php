<?php

namespace GuzzleExtension\Interfaces;

interface IHeaderBuilder
{
    /**
     * @param IRequest $request
     * @return array
     */
    public function build(IRequest $request);
}