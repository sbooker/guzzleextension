<?php

namespace GuzzleExtension\Interfaces;

use GuzzleExtension\Exceptions\ClientException;

interface IClient
{
    /**
     * @param IRequest $request
     * @param string $url
     * @throws ClientException
     * @return IResponse
     */
    public function send(IRequest $request, $url);
}