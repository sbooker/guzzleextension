<?php

namespace GuzzleExtension\Interfaces;

use GuzzleExtension\Exceptions\ParserException;
use Psr\Http\Message\ResponseInterface;

interface IResponseParser
{
    /**
     * @param ResponseInterface $response
     * @param string $responseClass
     * @return IResponse
     */
    public function parse(ResponseInterface $response, $responseClass);
}