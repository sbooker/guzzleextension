<?php

namespace GuzzleExtension\Interfaces;

interface IAuthRequest extends IRequest
{
    /**
     * @return string
     */
    public function getLogin();

    /**
     * @return string
     */
    public function getPassword();
}