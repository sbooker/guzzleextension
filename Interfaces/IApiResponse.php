<?php

namespace GuzzleExtension\Interfaces;

interface IApiResponse extends IResponse
{
    /**
     * @return int | string
     */
    public function getCode();

    /**
     * @return string
     */
    public function getMessage();
}