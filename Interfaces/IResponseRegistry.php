<?php

namespace GuzzleExtension\Interfaces;

use GuzzleExtension\Exceptions\RegistryException;

interface IResponseRegistry
{
    /**
     * @param IRequest $request
     * @throws RegistryException
     * @return string response class
     */
    public function getResponseClass(IRequest $request);
}