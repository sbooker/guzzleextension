<?php

namespace GuzzleExtension\Interfaces;

interface IRegistry
{
    /**
     * @param IRequest $request
     * @return IRequestBuilder
     */
    public function getRequestBuilder(IRequest $request);

    /**
     * @param IRequest $request
     * @return string
     */
    public function getResponseClass(IRequest $request);

    /**
     * @param IRequest $request
     * @return IResponseParser
     */
    public function getResponseParser(IRequest $request);
}