<?php

namespace GuzzleExtension\Interfaces;

use GuzzleExtension\Exceptions\RegistryException;

interface IResponseParserRegistry
{
    /**
     * @param string $responseClass
     * @throws RegistryException
     * @return IResponseParser
     */
    public function getParser($responseClass);
}