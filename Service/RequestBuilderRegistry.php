<?php

namespace GuzzleExtension\Service;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\IRequestBuilder;
use GuzzleExtension\Interfaces\IRequestBuilderRegistry;

final class RequestBuilderRegistry extends AbstractRegistry implements IRequestBuilderRegistry
{
    public function __construct(array $map = [])
    {
        parent::__construct(IRequest::class, IRequestBuilder::class, $map);
    }
    
    public function getBuilder(IRequest $request)
    {
        return $this->get(get_class($request));
    }

    protected function assertValue($value)
    {
        Assert::isObject($value);
        parent::assertValue($value);
    }
}