<?php

namespace GuzzleExtension\Service;

use GuzzleExtension\Exceptions\ClientException;
use GuzzleExtension\Interfaces\IClient;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\IRequestBuilder;
use GuzzleExtension\Interfaces\IResponseParser;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

abstract class AbstractClient implements IClient
{
    /** @var GuzzleClient */
    private $client;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(GuzzleClient $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param IRequest $request
     * @return IRequestBuilder
     */
    abstract protected function getRequestBuilder(IRequest $request);

    /**
     * @param IRequest $request
     * @return IResponseParser
     */
    abstract protected function getResponseParser(IRequest $request);

    /**
     * @param IRequest $request
     * @return string
     */
    abstract protected function getResponseClass(IRequest $request);

    final public function send(IRequest $request, $url)
    {
        try {
            $this->logger->info(sprintf('Request: "%s" to URL: %s', var_export($request, true), $url));

            $response =
                $this->getResponseParser($request)->parse(
                    $this->client->send(
                        $this->getRequestBuilder($request)->build($request, $url),
                        [ RequestOptions::ALLOW_REDIRECTS => false ]
                    ),
                    $this->getResponseClass($request)
                );

            $this->logger->info(sprintf('Response: "%s"', var_export($response, true)));

            return $response;
        } catch (GuzzleException $e) {
            throw new ClientException($e->getMessage(), $e->getCode(), $e);
        }
    }
}