<?php

namespace GuzzleExtension\Service;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IResponse;
use GuzzleExtension\Interfaces\IResponseParser;
use GuzzleExtension\Interfaces\IResponseParserRegistry;

final class ResponseParserRegistry extends AbstractRegistry implements IResponseParserRegistry
{
    public function __construct(array $map = [])
    {
        parent::__construct(IResponse::class, IResponseParser::class, $map);
    }

    public function getParser($responseClass)
    {
        return $this->get($responseClass);
    }

    protected function assertValue($value)
    {
        Assert::isObject($value);
        parent::assertValue($value);
    }

    protected function buildKey($key)
    {
        return $key;
    }
}