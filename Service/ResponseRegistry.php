<?php

namespace GuzzleExtension\Service;

use Base\Utils\Assert;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\IResponse;
use GuzzleExtension\Interfaces\IResponseRegistry;

final class ResponseRegistry extends AbstractRegistry implements IResponseRegistry
{
    public function __construct(array $map = [])
    {
        parent::__construct(IRequest::class, IResponse::class, $map);
    }

    public function getResponseClass(IRequest $request)
    {
        return $this->get(get_class($request));
    }

    protected function assertValue($value)
    {
        Assert::isString($value);
        parent::assertValue($value);
    }
}