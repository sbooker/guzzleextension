<?php

namespace GuzzleExtension\Service;

use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\IRequestBuilder;
use GuzzleExtension\Interfaces\IResponseParser;
use GuzzleHttp\Client as GuzzleClient;
use Psr\Log\LoggerInterface;

final class Client extends AbstractClient
{
    /** @var IRequestBuilder */
    private $requestBuilder;

    /** @var IResponseParser */
    private $responseParser;

    /** @var string */
    private $responseClass;

    public function __construct(
        GuzzleClient $client,
        LoggerInterface $logger,
        IRequestBuilder $requestBuilder,
        IResponseParser $responseParser,
        $responseClass
    ) {
        parent::__construct($client, $logger);
        $this->requestBuilder = $requestBuilder;
        $this->responseParser = $responseParser;
        $this->responseClass = $responseClass;
    }


    protected function getRequestBuilder(IRequest $request)
    {
        return $this->requestBuilder;
    }

    protected function getResponseParser(IRequest $request)
    {
        return $this->responseParser;
    }

    protected function getResponseClass(IRequest $request)
    {
        return $this->responseClass;
    }
}