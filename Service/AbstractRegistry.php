<?php

namespace GuzzleExtension\Service;

use Base\Abstracts\AbstractMap;
use Base\Utils\Assert;

abstract class AbstractRegistry extends AbstractMap
{
    /** @var string */
    private $keyClass;
    
    /** @var string */
    private $valueClass;
    
    public function __construct($keyClass, $valueClass, array $map = [])
    {
        $this->keyClass = $keyClass;
        $this->valueClass = $valueClass;
        parent::__construct($map);
    }

    final protected function assertKey($key)
    {
        Assert::isString($key);
        Assert::isInstance($key, $this->keyClass);
    }

    protected function assertValue($value)
    {
        Assert::isInstance($value, $this->valueClass);
    }
}