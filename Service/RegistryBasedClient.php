<?php

namespace GuzzleExtension\Service;

use GuzzleExtension\Interfaces\IRegistry;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleHttp\Client as GuzzleClient;
use Psr\Log\LoggerInterface;

final class RegistryBasedClient extends AbstractClient
{
    /** @var IRegistry */
    private $registry;

    public function __construct(GuzzleClient $client, LoggerInterface $logger, IRegistry $registry)
    {
        parent::__construct($client, $logger);
        $this->registry = $registry;
    }

    protected function getRequestBuilder(IRequest $request)
    {
        return $this->registry->getRequestBuilder($request);
    }

    protected function getResponseParser(IRequest $request)
    {
        return $this->registry->getResponseParser($request);
    }

    protected function getResponseClass(IRequest $request)
    {
        return $this->registry->getResponseClass($request);
    }
}