<?php

namespace GuzzleExtension\Service;

use GuzzleExtension\Interfaces\IRegistry;
use GuzzleExtension\Interfaces\IRequest;
use GuzzleExtension\Interfaces\IRequestBuilderRegistry;
use GuzzleExtension\Interfaces\IResponseParserRegistry;
use GuzzleExtension\Interfaces\IResponseRegistry;

final class Registry implements IRegistry
{
    /** @var IRequestBuilderRegistry */
    private $requestBuilderRegistry;

    /** @var IResponseRegistry */
    private $responseRegistry;

    /** @var IResponseParserRegistry */
    private $responseParserRegistry;

    public function __construct(
        IRequestBuilderRegistry $requestBuilderRegistry,
        IResponseRegistry $responseRegistry,
        IResponseParserRegistry $responseParserRegistry
    ) {
        $this->requestBuilderRegistry = $requestBuilderRegistry;
        $this->responseRegistry = $responseRegistry;
        $this->responseParserRegistry = $responseParserRegistry;
    }


    public function getRequestBuilder(IRequest $request)
    {
        return $this->requestBuilderRegistry->getBuilder($request);
    }

    public function getResponseClass(IRequest $request)
    {
        return $this->responseRegistry->getResponseClass($request);
    }

    public function getResponseParser(IRequest $request)
    {
        return
            $this->responseParserRegistry->getParser(
                $this->getResponseClass($request)
            );
    }
}